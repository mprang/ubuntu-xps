# ubuntu-xps

Dell XPS - Enable WiFi and Sound on Ubuntu 20.04 LTS  
  
This small repo contains Ansible Play to enable WiFi and Sound your Ubuntu 20.04 LTS upon a clean install. It requires minimum Kernel 5.10 and some Alsa configurations for sof-soundwire which will be also installed.

# Requirements
Connect your mobile phone to DELL XPS and enable USB Tethering for getting internet access.

It requires Git and Ansible:

```
sudo apt install ansible git
```

And clone this repository:

```
git clone https://gitlab.com/mprang/ubuntu-xps.git
cd ubuntu-xps
```

# Run this Play to prepare your Ubuntu 20.04

You need sudo permission to execute this Ansible Play:

```
ansible-playbook ubuntu-xps.yml -K
```

Restart your system afterwards and have fun with your DELL XPS. :)

# Known issue

- dmesg points out that something is wrong (timeout) with i2c of nvidia-gpu. In point of my view, it can be ignored because GeForce RTX 2060 has no USB Type-C interface.

